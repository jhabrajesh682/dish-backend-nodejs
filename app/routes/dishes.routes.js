const router = require("express").Router();
const dish = require("../controllers/dishes.controller");
const auth = require("../middlewares/auth");


const dishes = new dish();
router.get("/SASUrl/:blobName", dishes.getDishFile);

router.get("/", [auth],dishes.getAllDish);

router.post("/", [auth],dishes.createDishes);

router.post("/:id", [auth],dishes.getOneAndUpdateDishes);

router.get("/:id", [auth],dishes.getOneAndDeleteDish)



module.exports = router