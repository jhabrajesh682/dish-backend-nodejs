const dishSchema = require("../models/dishes.models");
const validate = require("../validators/dishes.validator");
let {
    upload,
    getfile,
    deleteFile
} = require('../helper/awsBucket');

class dishes { 

    async createDishes(req,res) { 

        console.log("req body======>🔥", req.body);
        let { error } = validate.validateAddDishes(req.body);

        if (error) { 
            res.status(400).send({
                message: "failed",
                result:error
            })
        }

        const sampleFile = req.files.orignalName;
        const buffer = sampleFile.data;
        const filename = sampleFile.name;
        let data = await upload(buffer, filename);

        let dishes = new dishSchema({
            dishName: req.body.dishName,
            ingredientsName: req.body.ingredientsName,
            ingredientsQuantity: req.body.ingredientsQuantity,
            ingredientsUnit: req.body.ingredientsUnit,
            steps: req.body.steps,
            photo: {
                orignalName: filename,
                blobName: data.data.key,
            }
        })

        await dishes.save();
        res.status(200).send({
            status: true,
            result:dishes
        })
    }

    async getOneAndUpdateDishes(req,res) { 
        
        let dishId = req.params.id
        console.log("dishId====>", dishId);
        let dish = await dishSchema.findById(dishId)
        
        if (!dish) { 
           return res.status(404).send({
                message: "not found",
                status:false
            })
        }

        let { error } = validate.validateUpdateDishes(req.body);
        if (error) {
            res.status(400).send({
                message: "failed",
                result:"error"
            })
        }
        let orignalName;
        let filename;
        let sampleFile;
       
        if (!req.files) {
            if (req.body.orignalName == 'No Change') {
                orignalName = dish.photo.orignalName;
                filename = dish.photo.blobName;
            }
        }
        if (req.files) {
            orignalName = req.files.orignalName.name;
            sampleFile = req.files.orignalName;
            const buffer = sampleFile.data;
            let data = await upload(buffer, orignalName);
            deleteFile(dish.photo.blobName);
            filename = data.data.key;
        }

        let updatedDishes = {
            dishName: req.body.dishName,
            ingredientsName: req.body.ingredientsName,
            ingredientsQuantity: req.body.ingredientsQuantity,
            ingredientsUnit: req.body.ingredientsUnit,
            steps: req.body.steps,
            photo: {
                orignalName: orignalName ,
                blobName: filename,
            }
        }
        let updatedDish = await dishSchema.findByIdAndUpdate(
            { _id: dish._id },
            updatedDishes
        );
        res.status(200).send({
            message: "dish successfully updated",
            result:updatedDish
        })
    }

    async getDishFile(req,res) { 
        let blob = req.params.blobName;

        let sasUrl = getfile(blob);
        res.status(200).send({
            url: sasUrl,
        });
    }

    async getAllDish(req,res) { 
        let limit
        let page
        if (req.query.limit) {
            limit = (parseInt(req.query.limit) ? parseInt(req.query.limit) : 10);
            page = req.query.page ? (parseInt(req.query.page) ? parseInt(req.query.page) : 1) : 1;
        }

        const createdAt = req.query.createdAt ? (req.query.createdAt == 'desc' ? -1 : 1) : 1
        const dish = await dishSchema.find({})
            .limit(limit)
            .skip((page - 1) * limit)
            .sort({ createdAt: createdAt })
            .lean()
        res.status(200).send({
            status: true,
            dish: dish
        })
    }

    async getOneAndDeleteDish(req,res) { 

        let dishId = req.params.id

        let dish = await dishSchema.findByIdAndRemove(dishId);
        let status = false
        if (status) {
            status = true
        }

        res.status(200).send({
            status: status,
            dish:dish
        })

    }
}

module.exports = dishes