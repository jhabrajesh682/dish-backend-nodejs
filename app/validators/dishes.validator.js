const joi = require("@hapi/joi");


function validateAddDishes(dishes) {
    let schema = joi.object({
        dishName: joi.string().required(),
        ingredientsName: joi.string().required(),
        ingredientsQuantity: joi.string().required(),
        ingredientsUnit: joi.string().required(),
        steps: joi.string().required(),
        photo: joi.string(),
        orignalName: joi.string(),
    })

    let result = schema.validate(dishes)
    return result
}



function validateUpdateDishes(dishes) {
    let schema = joi.object({
        dishName: joi.string(),
        ingredientsName: joi.string(),
        ingredientsQuantity: joi.string(),
        ingredientsUnit: joi.string(),
        steps: joi.string(),
        photo: joi.string(),
        orignalName: joi.string(),
    })

    let result = schema.validate(dishes)
    return result
}

module.exports.validateAddDishes = validateAddDishes;
module.exports.validateUpdateDishes = validateUpdateDishes;