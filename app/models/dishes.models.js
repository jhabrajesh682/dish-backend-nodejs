const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const dishesSchema = new Schema({

    dishName: {
        type: String,
        required: true
    },

    ingredientsName: {
        type: String,
        required: true
    },

    ingredientsQuantity: {
        type: String,
        required: true
    },

    ingredientsUnit: {
        type: String,
        required: true
    },

    steps: {
        type: String,
        required: true
    },

    photo: {
        type: Object,
        required: true
    },

}, {
    timestamps: true
})

module.exports = dishes = mongoose.model("dishes", dishesSchema);